from django.conf.urls import url
from django.conf.urls.static import static
from . import views

# Create your URL's here
urlpatterns = [
    url(r'^sign_up', views.sign_up, name='sign_up'),
    url(r'^login', views.login, name='login'),
    url(r'^forgot_password', views.forgot_password, name='forgot_password'),
] + static('/static/')
