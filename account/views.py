from django.shortcuts import render
from .forms import SignUp
from django.http import HttpResponse , JsonResponse

# Create your views here.


def sign_up(request):
    if request.method == 'GET':
        context = {'f': SignUp()}
        return render(request, 'account/auth/sign_up.html',context)
    else:
        f = SignUp(request.POST, request.FILES)
        if not f.is_valid():
            return JsonResponse({'errors': f.errors})
        else:
            f.save(commit=True)
            return HttpResponse('ok')


def login(request):
    return render(request, 'account/auth/login.html')

def forgot_password(request):
    return render(request, 'account/auth/forgot_password.html')