from django import forms
from .models import User
from django.forms import extras


class SignUp(forms.ModelForm):
    dob = forms.DateField(widget=extras.SelectDateWidget(years=range(1990,2016)))
    MALE = "M"
    FEMALE = "F"
    OTHERS = "O"
    SEX = (
        (MALE, 'Male'),
        (FEMALE, 'Female'),
        (OTHERS, 'Others'),
    )
    sex = forms.ChoiceField(choices=SEX, widget=forms.Select)

    class Meta:
        model = User
        fields = ('first_name','last_name','password','email','sex','dob')
