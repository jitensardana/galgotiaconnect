from __future__ import unicode_literals
import uuid

from django.db import models
from django.contrib.auth.models import AbstractUser


# Create your models here.

class User(AbstractUser):
    phone = models.CharField(max_length=15, blank=True, null=True)
    dob = models.DateField(max_length=8, null=True)
    MALE = "M"
    FEMALE = "F"
    OTHERS = "O"
    SEX = (
        (MALE, 'Male'),
        (FEMALE, 'Female'),
        (OTHERS, 'Others'),
    )
    sex = models.CharField(max_length=1, blank=False, null=False, choices=SEX)
    profile_pic = models.ImageField(upload_to='profile_pic/', blank=True, null=True)
    branch = models.CharField(max_length=255, null=True, blank=True)
    program = models.CharField(max_length=255, null=True, blank=True)
    city = models.CharField(max_length=35, null=True, blank=True)
    state = models.CharField(max_length=35, null=True, blank=True)
    country = models.CharField(max_length=70, null=False, default='India', blank=True)
    website = models.URLField(max_length=200, null=True, blank=True)
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
